package com.artcode.designpatterns.creational.builder;

public class Client {
    public static void main(String[] args) {
        User user1 = new User.UserBuilder("John", "Smith").build();
        User user2 = new User.UserBuilder("Mark", "Example")
                .address("Some address")
                .age(22)
                .build();
        System.out.println(user1);
        System.out.println(user2);
    }
}
