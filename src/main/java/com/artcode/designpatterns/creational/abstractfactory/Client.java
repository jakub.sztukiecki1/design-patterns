package com.artcode.designpatterns.creational.abstractfactory;

import com.artcode.designpatterns.creational.abstractfactory.animals.Animal;
import com.artcode.designpatterns.creational.abstractfactory.colors.Color;

public class Client {
    public static void main(String[] args) {
        AbstractFactory abstractFactory = FactoryProvider.getFactory("animal");
        Animal animal = (Animal) abstractFactory.create("dog");

        abstractFactory = FactoryProvider.getFactory("color");
        Color color = (Color) abstractFactory.create("red");

        System.out.println("A " + animal.getType() + " with " + color.getColor() + " color " + animal.makeSound());
    }
}
