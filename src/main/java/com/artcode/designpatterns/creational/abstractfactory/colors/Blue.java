package com.artcode.designpatterns.creational.abstractfactory.colors;

public class Blue implements Color {
    @Override
    public String getColor() {
        return "blue";
    }
}
