package com.artcode.designpatterns.creational.abstractfactory.colors;

public interface Color {
    String getColor();
}
