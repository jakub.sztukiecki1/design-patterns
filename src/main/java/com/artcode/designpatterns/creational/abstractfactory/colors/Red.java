package com.artcode.designpatterns.creational.abstractfactory.colors;

public class Red implements Color {
    @Override
    public String getColor() {
        return "red";
    }
}
