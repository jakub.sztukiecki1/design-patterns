package com.artcode.designpatterns.creational.abstractfactory.animals;

public interface Animal {
    String getType();

    String makeSound();
}
