package com.artcode.designpatterns.creational.abstractfactory.animals;

public class Dog implements Animal {
    @Override
    public String getType() {
        return "Dog";
    }

    @Override
    public String makeSound() {
        return "barks";
    }
}
