package com.artcode.designpatterns.creational.abstractfactory;

import com.artcode.designpatterns.creational.abstractfactory.factories.AnimalFactory;
import com.artcode.designpatterns.creational.abstractfactory.factories.ColorFactory;

public class FactoryProvider {
    public static AbstractFactory getFactory(String factoryType) {
        if ("animal".equalsIgnoreCase(factoryType)) {
            return new AnimalFactory();
        } else if ("color".equalsIgnoreCase(factoryType)) {
            return new ColorFactory();
        } else {
            throw new AbstractFactory.UnknownTypeException();
        }
    }
}
