package com.artcode.designpatterns.creational.abstractfactory.animals;

public class Duck implements Animal {
    @Override
    public String getType() {
        return "Duck";
    }

    @Override
    public String makeSound() {
        return "squeeks";
    }
}
