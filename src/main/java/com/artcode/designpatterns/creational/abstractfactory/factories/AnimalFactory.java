package com.artcode.designpatterns.creational.abstractfactory.factories;

import com.artcode.designpatterns.creational.abstractfactory.AbstractFactory;
import com.artcode.designpatterns.creational.abstractfactory.animals.Animal;
import com.artcode.designpatterns.creational.abstractfactory.animals.Dog;
import com.artcode.designpatterns.creational.abstractfactory.animals.Duck;

public class AnimalFactory implements AbstractFactory<Animal> {
    @Override
    public Animal create(String type) {
        if ("dog".equalsIgnoreCase(type)) {
            return new Dog();
        } else if ("duck".equalsIgnoreCase(type)) {
            return new Duck();
        } else {
            throw new UnknownTypeException();
        }
    }
}
