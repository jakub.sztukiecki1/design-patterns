package com.artcode.designpatterns.creational.abstractfactory;

public interface AbstractFactory<T> {
    T create(String type);

    class UnknownTypeException extends RuntimeException {

    }
}
