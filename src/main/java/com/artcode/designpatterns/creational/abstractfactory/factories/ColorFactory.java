package com.artcode.designpatterns.creational.abstractfactory.factories;

import com.artcode.designpatterns.creational.abstractfactory.AbstractFactory;
import com.artcode.designpatterns.creational.abstractfactory.colors.Blue;
import com.artcode.designpatterns.creational.abstractfactory.colors.Color;
import com.artcode.designpatterns.creational.abstractfactory.colors.Red;

public class ColorFactory implements AbstractFactory<Color> {
    @Override
    public Color create(String type) {
        if ("red".equalsIgnoreCase(type)) {
            return new Red();
        } else if ("blue".equalsIgnoreCase(type)) {
            return new Blue();
        } else {
            throw new UnknownTypeException();
        }
    }
}
