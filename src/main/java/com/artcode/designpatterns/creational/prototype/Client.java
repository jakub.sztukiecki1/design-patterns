package com.artcode.designpatterns.creational.prototype;

public class Client {
    public static void main(String[] args) throws CloneNotSupportedException {
        Employee employee = new Employee("John", "Doe", 10000d);
        Employee employee2 = (Employee) employee.clone();

        System.out.println(employee);
        System.out.println(employee2);

        System.out.println("employee == employee2 -> " + (employee == employee2));
    }
}
