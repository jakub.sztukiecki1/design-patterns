package com.artcode.designpatterns.creational.prototype;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Employee implements Cloneable {

    private String name;
    private String surname;
    private double salary;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
