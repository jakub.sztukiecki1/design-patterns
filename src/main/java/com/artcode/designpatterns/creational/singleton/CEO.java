package com.artcode.designpatterns.creational.singleton;

public class CEO {
    private String name;
    private String surname;
    private static CEO instance = new CEO("John", "Doe");

    private CEO(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public static CEO getInstance() {
        return instance;
    }
}
