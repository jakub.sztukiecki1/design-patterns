package com.artcode.designpatterns.creational.singleton;

public class Client {
    public static void main(String[] args) {
        CEO instance = CEO.getInstance();
        CEO sameInstance = CEO.getInstance();
        System.out.println("instance == sameInstance -> " + (instance == sameInstance));
    }
}
