package com.artcode.designpatterns.creational.factorymethod;

import com.artcode.designpatterns.creational.factorymethod.products.Circle;
import com.artcode.designpatterns.creational.factorymethod.products.Rectangle;
import com.artcode.designpatterns.creational.factorymethod.products.Shape;

public class ShapeFactory {
    public static Shape createShape(Shape.ShapeType shapeType) {
        switch (shapeType) {
            case CIRCLE:
                return new Circle();
            case RECTANGLE:
                return new Rectangle();
            default:
                throw new ShapeTypeNotFoundException();
        }
    }

    private static class ShapeTypeNotFoundException extends RuntimeException {

    }
}
