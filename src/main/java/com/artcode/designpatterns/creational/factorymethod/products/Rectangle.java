package com.artcode.designpatterns.creational.factorymethod.products;

public class Rectangle extends Shape {
    @Override
    public void draw() {
        System.out.println("Rectangle is drawn");
    }
}
