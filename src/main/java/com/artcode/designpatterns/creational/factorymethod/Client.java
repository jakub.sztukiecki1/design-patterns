package com.artcode.designpatterns.creational.factorymethod;

import com.artcode.designpatterns.creational.factorymethod.products.Shape;

public class Client {
    public static void main(String[] args) {
        Shape circle = ShapeFactory.createShape(Shape.ShapeType.CIRCLE);
        Shape rectangle = ShapeFactory.createShape(Shape.ShapeType.RECTANGLE);
        circle.draw();
        rectangle.draw();
    }
}
