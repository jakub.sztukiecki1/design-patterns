package com.artcode.designpatterns.creational.factorymethod.products;

public abstract class Shape {
    public abstract void draw();

    public static enum ShapeType {
        CIRCLE, RECTANGLE
    }
}
