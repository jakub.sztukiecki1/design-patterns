package com.artcode.designpatterns.creational.factorymethod.products;

public class Circle extends Shape {
    @Override
    public void draw() {
        System.out.println("Circle is drawn");
    }
}
