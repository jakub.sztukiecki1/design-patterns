package com.artcode.designpatterns.behavioral.templatemethod;

public abstract class HouseTemplate {
    public final void buildHouse() {
        buildFoundation();
        buildPillars();
        buildWalls();
        buildWindows();
        completeHouse();
    }

    protected abstract void buildPillars();

    protected abstract void buildWalls();

    private void buildFoundation() {
        System.out.println("Building cement foundation");
    }

    private void buildWindows() {
        System.out.println("Building glass windows");
    }

    private void completeHouse() {
        System.out.println("House is finished");
    }

}
