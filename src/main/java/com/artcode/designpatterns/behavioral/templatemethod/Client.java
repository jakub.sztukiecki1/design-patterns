package com.artcode.designpatterns.behavioral.templatemethod;

import com.artcode.designpatterns.behavioral.templatemethod.concrete.CementHouse;
import com.artcode.designpatterns.behavioral.templatemethod.concrete.WoodenHouse;

public class Client {
    public static void main(String[] args) {
        HouseTemplate cementHouse = new CementHouse();
        HouseTemplate woodenHouse = new WoodenHouse();

        cementHouse.buildHouse();
        woodenHouse.buildHouse();
    }
}
