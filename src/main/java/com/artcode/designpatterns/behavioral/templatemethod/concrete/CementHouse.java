package com.artcode.designpatterns.behavioral.templatemethod.concrete;

import com.artcode.designpatterns.behavioral.templatemethod.HouseTemplate;

public class CementHouse extends HouseTemplate {
    @Override
    protected void buildPillars() {
        System.out.println("Building cement pillars");
    }

    @Override
    protected void buildWalls() {
        System.out.println("Building cement walls");
    }
}
