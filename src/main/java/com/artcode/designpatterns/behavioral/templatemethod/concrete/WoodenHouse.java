package com.artcode.designpatterns.behavioral.templatemethod.concrete;

import com.artcode.designpatterns.behavioral.templatemethod.HouseTemplate;

public class WoodenHouse extends HouseTemplate {
    @Override
    protected void buildPillars() {
        System.out.println("Building wooden pillars");
    }

    @Override
    protected void buildWalls() {
        System.out.println("Building wooden walls");
    }
}
