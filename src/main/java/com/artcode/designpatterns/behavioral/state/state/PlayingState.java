package com.artcode.designpatterns.behavioral.state.state;

import com.artcode.designpatterns.behavioral.state.MP3Player;

public class PlayingState implements State {
    @Override
    public void pressPlay(MP3Player player) {
        player.setState(new StandbyState());
    }
}
