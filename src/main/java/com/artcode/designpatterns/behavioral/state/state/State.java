package com.artcode.designpatterns.behavioral.state.state;

import com.artcode.designpatterns.behavioral.state.MP3Player;

public interface State {
    void pressPlay(MP3Player player);
}
