package com.artcode.designpatterns.behavioral.state;

import com.artcode.designpatterns.behavioral.state.state.State;

public class MP3Player {
    private State state;

    public void pressPlay() {
        this.state.pressPlay(this);
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}
