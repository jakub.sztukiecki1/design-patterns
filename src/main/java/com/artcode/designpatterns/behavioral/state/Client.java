package com.artcode.designpatterns.behavioral.state;

import com.artcode.designpatterns.behavioral.state.state.StandbyState;

public class Client {
    public static void main(String[] args) {
        MP3Player mp3Player = new MP3Player();
        mp3Player.setState(new StandbyState());
        System.out.println("Initial state: " + mp3Player.getState());

        mp3Player.pressPlay();
        System.out.println("After first press: " + mp3Player.getState());

        mp3Player.pressPlay();
        System.out.println("After second press: " + mp3Player.getState());
    }
}
