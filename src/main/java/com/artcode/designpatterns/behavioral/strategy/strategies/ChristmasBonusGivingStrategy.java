package com.artcode.designpatterns.behavioral.strategy.strategies;

public class ChristmasBonusGivingStrategy implements BonusGivingStrategy {

    @Override
    public double giveBonus(double salary) {
        return 1000;
    }
}
