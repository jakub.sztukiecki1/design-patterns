package com.artcode.designpatterns.behavioral.strategy;

import com.artcode.designpatterns.behavioral.strategy.strategies.BonusGivingStrategy;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Employee {
    private String name;
    private double salary;
    private BonusGivingStrategy bonusGivingStrategy;

    public void giveBonus() {
        System.out.println("Bonus given to " + name + ": " + bonusGivingStrategy.giveBonus(salary));
    }
}
