package com.artcode.designpatterns.behavioral.strategy.strategies;

public class GenerousBonusGivingStrategy implements BonusGivingStrategy {
    @Override
    public double giveBonus(double salary) {
        return salary * 0.5;
    }
}
