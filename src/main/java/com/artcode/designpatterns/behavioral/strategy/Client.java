package com.artcode.designpatterns.behavioral.strategy;

import com.artcode.designpatterns.behavioral.strategy.strategies.ChristmasBonusGivingStrategy;
import com.artcode.designpatterns.behavioral.strategy.strategies.GenerousBonusGivingStrategy;
import com.artcode.designpatterns.behavioral.strategy.strategies.StingyBonusGivingStrategy;

public class Client {
    public static void main(String[] args) {
        Employee e1 = new Employee("Jan Nowak", 2000.0, new ChristmasBonusGivingStrategy());
        Employee e2 = new Employee("Marian Kowalski", 3000.0, new GenerousBonusGivingStrategy());
        Employee e3 = new Employee("Bartłomiej Mickiewicz", 4000.0, new StingyBonusGivingStrategy());
        Employee e4 = new Employee("Tomasz Testowy", 5000.0, salary -> salary * 0.8);

        e1.giveBonus();
        e2.giveBonus();
        e3.giveBonus();
        e4.giveBonus();
    }
}
