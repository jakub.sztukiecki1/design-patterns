package com.artcode.designpatterns.behavioral.strategy.strategies;

public interface BonusGivingStrategy {
    double giveBonus(double salary);
}
