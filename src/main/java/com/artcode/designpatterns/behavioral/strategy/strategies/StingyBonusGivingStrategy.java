package com.artcode.designpatterns.behavioral.strategy.strategies;

public class StingyBonusGivingStrategy implements BonusGivingStrategy {
    @Override
    public double giveBonus(double salary) {
        return salary * 0.1;
    }
}
