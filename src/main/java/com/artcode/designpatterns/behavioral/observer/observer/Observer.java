package com.artcode.designpatterns.behavioral.observer.observer;

public interface Observer {
    void notify(Object object);
}
