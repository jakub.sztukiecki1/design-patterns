package com.artcode.designpatterns.behavioral.observer.subject;

import com.artcode.designpatterns.behavioral.observer.observer.Observer;

public interface Observable {
    void addObserver(Observer observer);

    void removeObserver(Observer observer);
}
