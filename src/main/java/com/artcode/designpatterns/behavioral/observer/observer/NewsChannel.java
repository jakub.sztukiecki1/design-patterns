package com.artcode.designpatterns.behavioral.observer.observer;


public class NewsChannel implements Observer {

    private String channelName;

    public NewsChannel(String channelName) {
        this.channelName = channelName;
    }

    @Override
    public void notify(Object object) {
        System.out.println(channelName + ": " + object);
    }
}
