package com.artcode.designpatterns.behavioral.observer;

import com.artcode.designpatterns.behavioral.observer.observer.NewsChannel;
import com.artcode.designpatterns.behavioral.observer.observer.Observer;
import com.artcode.designpatterns.behavioral.observer.subject.NewsAgency;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Client {
    public static void main(String[] args) {
        NewsAgency newsAgency = new NewsAgency();

        List<Observer> newsChannels = new ArrayList<>();
        IntStream.of(1, 2, 3).forEach(e -> newsChannels.add(new NewsChannel("Channel " + e)));

        newsChannels.forEach(newsAgency::addObserver);

        newsAgency.setNews("Awesome news");
        newsAgency.setNews("Other news");

        newsAgency.removeObserver(newsChannels.get(0));

        newsAgency.setNews("Best news");
    }
}
