package com.artcode.designpatterns.behavioral.observer.subject;

import com.artcode.designpatterns.behavioral.observer.observer.Observer;
import com.artcode.designpatterns.behavioral.observer.subject.Observable;

import java.util.HashSet;
import java.util.Set;

public class NewsAgency implements Observable {

    private Set<Observer> observers = new HashSet<>();
    private String news;

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    public void setNews(String news) {
        this.news = news;
        observers.forEach(e -> e.notify(news));
    }

    public String getNews() {
        return news;
    }
}
