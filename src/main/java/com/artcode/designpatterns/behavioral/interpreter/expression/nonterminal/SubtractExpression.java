package com.artcode.designpatterns.behavioral.interpreter.expression.nonterminal;

import com.artcode.designpatterns.behavioral.interpreter.expression.Expression;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SubtractExpression implements Expression {

    private Expression number1;
    private Expression number2;

    @Override
    public int interpret() {
        return number1.interpret() - number2.interpret();
    }
}
