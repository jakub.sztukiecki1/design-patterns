package com.artcode.designpatterns.behavioral.interpreter;

import com.artcode.designpatterns.behavioral.interpreter.expression.Expression;
import com.artcode.designpatterns.behavioral.interpreter.expression.nonterminal.AddExpression;
import com.artcode.designpatterns.behavioral.interpreter.expression.nonterminal.SubtractExpression;
import com.artcode.designpatterns.behavioral.interpreter.expression.terminal.NumberExpression;

public class Client {
    public static void main(String[] args) {
        SubtractExpression subtractExpression = new SubtractExpression(new NumberExpression(5), new NumberExpression(3));
        AddExpression addExpression = new AddExpression(subtractExpression, new NumberExpression(7));
        // ((5 - 3) + 7) + 4
        Expression expression = new AddExpression(addExpression, new NumberExpression(4));

        System.out.println(expression.interpret());
    }
}
