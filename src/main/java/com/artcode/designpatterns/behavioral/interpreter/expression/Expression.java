package com.artcode.designpatterns.behavioral.interpreter.expression;

public interface Expression {
    int interpret();
}
