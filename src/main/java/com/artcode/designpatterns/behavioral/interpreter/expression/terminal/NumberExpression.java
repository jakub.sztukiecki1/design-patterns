package com.artcode.designpatterns.behavioral.interpreter.expression.terminal;

import com.artcode.designpatterns.behavioral.interpreter.expression.Expression;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class NumberExpression implements Expression {
    private int number;

    @Override
    public int interpret() {
        return number;
    }
}
