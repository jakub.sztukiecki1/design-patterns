package com.artcode.designpatterns.behavioral.memento;

public class TextFieldMemento {
    private String text;

    public TextFieldMemento(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
