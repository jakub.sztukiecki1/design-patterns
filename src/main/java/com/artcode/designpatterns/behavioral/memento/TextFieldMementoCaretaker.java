package com.artcode.designpatterns.behavioral.memento;

import java.util.Stack;

public class TextFieldMementoCaretaker {
    private Stack<TextFieldMemento> mementos = new Stack<>();

    public void addMemento(TextFieldMemento memento) {
        mementos.add(memento);
    }

    public TextFieldMemento getMemento() {
        return mementos.pop();
    }
}
