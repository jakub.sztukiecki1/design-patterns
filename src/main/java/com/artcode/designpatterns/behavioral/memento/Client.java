package com.artcode.designpatterns.behavioral.memento;

public class Client {
    public static void main(String[] args) {
        TextFieldMementoCaretaker caretaker = new TextFieldMementoCaretaker();

        TextField textField = new TextField();
        textField.setText("text1");
        caretaker.addMemento(textField.save());
        System.out.println(textField);

        textField.setText("text2");
        System.out.println("Before restore: " + textField);

        textField.restore(caretaker.getMemento());
        System.out.println("After restore: " + textField);
    }
}
