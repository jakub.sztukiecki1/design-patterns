package com.artcode.designpatterns.behavioral.memento;

public class TextField {
    private String text;

    public void setText(String text) {
        this.text = text;
    }

    public TextFieldMemento save() {
        return new TextFieldMemento(text);
    }

    public void restore(TextFieldMemento memento) {
        this.text = memento.getText();
    }

    @Override
    public String toString() {
        return "TextField{" +
                "text='" + text + '\'' +
                '}';
    }
}
