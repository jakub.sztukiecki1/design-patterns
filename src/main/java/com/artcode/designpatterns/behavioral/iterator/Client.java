package com.artcode.designpatterns.behavioral.iterator;

import com.artcode.designpatterns.behavioral.iterator.collection.ChannelCollection;
import com.artcode.designpatterns.behavioral.iterator.collection.ChannelCollectionImpl;
import com.artcode.designpatterns.behavioral.iterator.model.Channel;
import com.artcode.designpatterns.behavioral.iterator.model.ChannelTypeEnum;

public class Client {
    public static void main(String[] args) {
        ChannelCollection channels = populateChannels();
        ChannelIterator allIterator = channels.iterator(ChannelTypeEnum.ALL);

        System.out.println("All channels:");
        while (allIterator.hasNext()) {
            Channel c = allIterator.next();
            System.out.println(c);
        }

        ChannelIterator germanIterator = channels.iterator(ChannelTypeEnum.GERMAN);

        System.out.println("German channels:");
        while (germanIterator.hasNext()) {
            Channel c = germanIterator.next();
            System.out.println(c);
        }
    }

    private static ChannelCollection populateChannels() {
        ChannelCollection channels = new ChannelCollectionImpl();
        channels.addChannel(new Channel(98.5f, ChannelTypeEnum.ENGLISH));
        channels.addChannel(new Channel(99.5f, ChannelTypeEnum.GERMAN));
        channels.addChannel(new Channel(100.5f, ChannelTypeEnum.FRENCH));
        channels.addChannel(new Channel(101.5f, ChannelTypeEnum.ENGLISH));
        channels.addChannel(new Channel(102.5f, ChannelTypeEnum.GERMAN));
        channels.addChannel(new Channel(103.5f, ChannelTypeEnum.FRENCH));
        channels.addChannel(new Channel(104.5f, ChannelTypeEnum.ENGLISH));
        channels.addChannel(new Channel(105.5f, ChannelTypeEnum.GERMAN));
        channels.addChannel(new Channel(106.5f, ChannelTypeEnum.FRENCH));
        return channels;
    }
}
