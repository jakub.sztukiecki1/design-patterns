package com.artcode.designpatterns.behavioral.iterator.collection;

import com.artcode.designpatterns.behavioral.iterator.model.Channel;
import com.artcode.designpatterns.behavioral.iterator.ChannelIterator;
import com.artcode.designpatterns.behavioral.iterator.model.ChannelTypeEnum;

public interface ChannelCollection {
    void addChannel(Channel c);

    void removeChannel(Channel c);

    ChannelIterator iterator(ChannelTypeEnum type);
}
