package com.artcode.designpatterns.behavioral.iterator;

import com.artcode.designpatterns.behavioral.iterator.model.Channel;

public interface ChannelIterator {
    boolean hasNext();

    Channel next();
}
