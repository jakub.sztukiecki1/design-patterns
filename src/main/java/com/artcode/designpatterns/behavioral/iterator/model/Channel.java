package com.artcode.designpatterns.behavioral.iterator.model;

import lombok.Data;

@Data
public class Channel {
    private float frequency;
    private ChannelTypeEnum type;

    public Channel(float frequency, ChannelTypeEnum type) {
        this.frequency = frequency;
        this.type = type;
    }
}
