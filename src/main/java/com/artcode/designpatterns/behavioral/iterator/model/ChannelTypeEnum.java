package com.artcode.designpatterns.behavioral.iterator.model;

public enum ChannelTypeEnum {
    ENGLISH, GERMAN, FRENCH, ALL
}
