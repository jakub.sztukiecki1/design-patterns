package com.artcode.designpatterns.behavioral.mediator.colleagues;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Background {
    private String colorRGB;

    public void setColorRGB(String colorRGB) {
        this.colorRGB = colorRGB;
    }

    public String getColorRGB() {
        return colorRGB;
    }
}
