package com.artcode.designpatterns.behavioral.mediator.colleagues;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Button {
    boolean clicked;

    public void click() {
        this.clicked = !this.clicked;
    }

    public boolean isClicked() {
        return clicked;
    }
}
