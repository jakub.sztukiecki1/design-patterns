package com.artcode.designpatterns.behavioral.mediator;

import com.artcode.designpatterns.behavioral.mediator.colleagues.Background;
import com.artcode.designpatterns.behavioral.mediator.colleagues.Button;
import com.artcode.designpatterns.behavioral.mediator.colleagues.Text;

public class Client {
    public static void main(String[] args) {
        Button button = new Button(false);
        Background background = new Background("a2a2a2");
        Text text = new Text("Button not clicked");
        DialogMediator dialogMediator = DialogMediator.of(button, background, text);
        dialogMediator.clickButton();

        System.out.println(button.isClicked());
        System.out.println(background.getColorRGB());
        System.out.println(text.getText());
    }
}
