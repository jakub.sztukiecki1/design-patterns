package com.artcode.designpatterns.behavioral.mediator;

import com.artcode.designpatterns.behavioral.mediator.colleagues.Button;
import com.artcode.designpatterns.behavioral.mediator.colleagues.Background;
import com.artcode.designpatterns.behavioral.mediator.colleagues.Text;

public class DialogMediator {
    private Button button;
    private Background background;
    private Text text;

    private DialogMediator(Button button, Background background, Text text) {
        this.button = button;
        this.background = background;
        this.text = text;
    }

    public static DialogMediator of(Button button, Background background, Text text) {
        return new DialogMediator(button, background, text);
    }

    public void clickButton() {
        button.click();
        if (button.isClicked()) {
            background.setColorRGB("f1f1f1");
            text.setText("Button clicked");
        } else {
            background.setColorRGB("a2a2a2");
            text.setText("Button not clicked");
        }
    }
}
