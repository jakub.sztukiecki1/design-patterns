package com.artcode.designpatterns.behavioral.mediator.colleagues;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Text {
    private String text;

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
