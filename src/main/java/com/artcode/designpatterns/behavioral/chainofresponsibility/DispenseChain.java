package com.artcode.designpatterns.behavioral.chainofresponsibility;

public abstract class DispenseChain {
    protected DispenseChain successor;

    public void setNextChain(DispenseChain dispenseChain) {
        this.successor = dispenseChain;
    }

    public void dispense(Long amount) {
        if (amount >= getDispenserAmount()) {
            long num = amount / getDispenserAmount();
            long remainder = amount % getDispenserAmount();
            System.out.println("Dispensing " + num + " " + getDispenserAmount() + "$ note");
            if (remainder != 0) {
                dispense(remainder);
            }
        } else {
            this.successor.dispense(amount);
        }
    }

    public abstract long getDispenserAmount();
}
