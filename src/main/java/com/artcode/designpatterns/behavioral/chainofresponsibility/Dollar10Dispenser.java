package com.artcode.designpatterns.behavioral.chainofresponsibility;

public class Dollar10Dispenser extends DispenseChain {

    @Override
    public long getDispenserAmount() {
        return 10;
    }
}
