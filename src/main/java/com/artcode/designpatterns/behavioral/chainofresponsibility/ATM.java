package com.artcode.designpatterns.behavioral.chainofresponsibility;

public class ATM {

    private DispenseChain chain1;

    public ATM() {
        this.chain1 = new Dollar50Dispenser();
        Dollar20Dispenser chain2 = new Dollar20Dispenser();
        chain1.setNextChain(chain2);
        chain2.setNextChain(new Dollar10Dispenser());
    }

    public static void main(String[] args) {
        ATM atm = new ATM();
        long amountToDispense = 180;
        System.out.println("Amount to dispense: " + amountToDispense + "$");
        atm.chain1.dispense(amountToDispense);
    }
}
