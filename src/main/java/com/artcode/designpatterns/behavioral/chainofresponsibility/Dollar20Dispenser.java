package com.artcode.designpatterns.behavioral.chainofresponsibility;

public class Dollar20Dispenser extends DispenseChain {

    @Override
    public long getDispenserAmount() {
        return 20;
    }
}
