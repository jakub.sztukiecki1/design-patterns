package com.artcode.designpatterns.behavioral.chainofresponsibility;

public class Dollar50Dispenser extends DispenseChain {

    @Override
    public long getDispenserAmount() {
        return 50;
    }
}
