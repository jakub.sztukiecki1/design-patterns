package com.artcode.designpatterns.behavioral.visitor.visitor;

import com.artcode.designpatterns.behavioral.visitor.element.Circle;
import com.artcode.designpatterns.behavioral.visitor.element.Rectangle;

public interface Visitor {
    String visitCircle(Circle circle);

    String visitRectangle(Rectangle rectangle);
}
