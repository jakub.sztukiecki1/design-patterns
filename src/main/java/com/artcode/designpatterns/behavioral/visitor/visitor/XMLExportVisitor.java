package com.artcode.designpatterns.behavioral.visitor.visitor;

import com.artcode.designpatterns.behavioral.visitor.element.Circle;
import com.artcode.designpatterns.behavioral.visitor.element.Rectangle;

public class XMLExportVisitor implements Visitor {
    @Override
    public String visitCircle(Circle circle) {
        return "<circle>" + "\n" +
                "    <x>" + circle.getX() + "</x>" + "\n" +
                "    <y>" + circle.getY() + "</y>" + "\n" +
                "    <radius>" + circle.getRadius() + "</radius>" + "\n" +
                "</circle>";
    }

    @Override
    public String visitRectangle(Rectangle rectangle) {
        return "<rectangle>" + "\n" +
                "    <x>" + rectangle.getX() + "</x>" + "\n" +
                "    <y>" + rectangle.getY() + "</y>" + "\n" +
                "    <width>" + rectangle.getWidth() + "</width>" + "\n" +
                "    <height>" + rectangle.getHeight() + "</height>" + "\n" +
                "</rectangle>";
    }
}
