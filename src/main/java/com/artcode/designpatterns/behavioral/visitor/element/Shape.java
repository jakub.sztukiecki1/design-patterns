package com.artcode.designpatterns.behavioral.visitor.element;

import com.artcode.designpatterns.behavioral.visitor.visitor.Visitor;

public interface Shape {
    String accept(Visitor visitor);
}
