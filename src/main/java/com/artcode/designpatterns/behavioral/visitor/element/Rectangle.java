package com.artcode.designpatterns.behavioral.visitor.element;

import com.artcode.designpatterns.behavioral.visitor.visitor.Visitor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Rectangle implements Shape {
    private int x;
    private int y;
    private int width;
    private int height;

    @Override
    public String accept(Visitor visitor) {
        return visitor.visitRectangle(this);
    }
}
