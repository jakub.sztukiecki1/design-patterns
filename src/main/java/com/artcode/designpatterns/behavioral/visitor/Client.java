package com.artcode.designpatterns.behavioral.visitor;

import com.artcode.designpatterns.behavioral.visitor.element.Circle;
import com.artcode.designpatterns.behavioral.visitor.element.Rectangle;
import com.artcode.designpatterns.behavioral.visitor.element.Shape;
import com.artcode.designpatterns.behavioral.visitor.visitor.XMLExportVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Client {
    public static void main(String[] args) {
        List<Shape> shapes = new ArrayList<>();

        IntStream.of(1, 2, 3).forEach(e -> shapes.add(Circle.builder().x(e).y(2 * e).radius(3 * e).build()));
        IntStream.of(1, 2, 3).forEach(e -> shapes.add(Rectangle.builder().x(e).y(2 * e).width(3 * e).height(4 * e).build()));

        XMLExportVisitor visitor = new XMLExportVisitor();

        shapes.forEach(s -> System.out.println(s.accept(visitor)));
    }
}
