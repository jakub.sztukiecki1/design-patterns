package com.artcode.designpatterns.behavioral.command.invoker;

import com.artcode.designpatterns.behavioral.command.Command;

public class RemoteControl {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void pressButton() {
        command.execute();
    }
}
