package com.artcode.designpatterns.behavioral.command.concretecommands;

import com.artcode.designpatterns.behavioral.command.Command;
import com.artcode.designpatterns.behavioral.command.receiver.Light;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LightOnCommand implements Command {
    private Light light;

    @Override
    public void execute() {
        light.switchOn();
    }
}
