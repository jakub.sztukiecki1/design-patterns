package com.artcode.designpatterns.behavioral.command;

import com.artcode.designpatterns.behavioral.command.concretecommands.LightOffCommand;
import com.artcode.designpatterns.behavioral.command.concretecommands.LightOnCommand;
import com.artcode.designpatterns.behavioral.command.invoker.RemoteControl;
import com.artcode.designpatterns.behavioral.command.receiver.Light;

public class Client {
    public static void main(String[] args) {
        Light light = new Light();
        Command lightOnCommand = new LightOnCommand(light);
        Command lightOffCommand = new LightOffCommand(light);

        RemoteControl remoteControl = new RemoteControl();
        remoteControl.setCommand(lightOnCommand);
        remoteControl.pressButton();

        remoteControl.setCommand(lightOffCommand);
        remoteControl.pressButton();
    }
}
