package com.artcode.designpatterns.behavioral.command.receiver;

public class Light {
    private boolean on;

    public void switchOn() {
        on = true;
        System.out.println("The light is turned on");
    }

    public void switchOff() {
        on = false;
        System.out.println("The light is turned off");
    }
}
