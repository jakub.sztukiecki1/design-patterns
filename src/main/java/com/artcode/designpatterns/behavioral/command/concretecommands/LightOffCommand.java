package com.artcode.designpatterns.behavioral.command.concretecommands;

import com.artcode.designpatterns.behavioral.command.Command;
import com.artcode.designpatterns.behavioral.command.receiver.Light;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LightOffCommand implements Command {
    private Light light;

    @Override
    public void execute() {
        light.switchOff();
    }
}
