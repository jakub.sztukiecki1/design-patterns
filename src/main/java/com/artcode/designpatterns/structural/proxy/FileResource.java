package com.artcode.designpatterns.structural.proxy;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class FileResource implements Resource {

    private String fileName;

    @Override
    public String getResource() {
        return "Opening file " + fileName;
    }
}
