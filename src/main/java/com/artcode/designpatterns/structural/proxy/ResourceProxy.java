package com.artcode.designpatterns.structural.proxy;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ResourceProxy implements Resource {

    private Resource resource;

    @Override
    public String getResource() {
        if (hasAccess()) {
            return resource.getResource();
        } else {
            return "No access to the resource";
        }
    }

    private boolean hasAccess() {
        long number = Math.round(Math.random());
        return number % 2 == 0;
    }
}
