package com.artcode.designpatterns.structural.proxy;

public interface Resource {
    String getResource();
}
