package com.artcode.designpatterns.structural.proxy;

import java.util.stream.IntStream;

public class Client {
    public static void main(String[] args) {
        ResourceProxy proxy = new ResourceProxy(new FileResource("secret.txt"));

        IntStream.range(0, 10).forEach(e -> System.out.println(proxy.getResource()));
    }
}
