package com.artcode.designpatterns.structural.bridge.implementor;

public interface Color {
    void applyColor();
}
