package com.artcode.designpatterns.structural.bridge.abstraction;

import com.artcode.designpatterns.structural.bridge.implementor.Color;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class Shape {
    protected Color color;

    public abstract void applyColor();
}
