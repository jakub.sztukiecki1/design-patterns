package com.artcode.designpatterns.structural.bridge.abstraction;

import com.artcode.designpatterns.structural.bridge.implementor.Color;

public class Triangle extends Shape {
    public Triangle(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Triangle filled with color");
        super.color.applyColor();
    }
}
