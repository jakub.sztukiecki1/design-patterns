package com.artcode.designpatterns.structural.bridge.implementor;

public class BlueColor implements Color {
    @Override
    public void applyColor() {
        System.out.println(" blue.");
    }
}
