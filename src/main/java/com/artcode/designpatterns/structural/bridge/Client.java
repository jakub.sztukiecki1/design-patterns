package com.artcode.designpatterns.structural.bridge;

import com.artcode.designpatterns.structural.bridge.abstraction.Shape;
import com.artcode.designpatterns.structural.bridge.abstraction.Triangle;
import com.artcode.designpatterns.structural.bridge.implementor.BlueColor;

public class Client {
    public static void main(String[] args) {
        Shape shape = new Triangle(new BlueColor());
        shape.applyColor();
    }
}
