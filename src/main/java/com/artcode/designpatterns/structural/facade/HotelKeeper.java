package com.artcode.designpatterns.structural.facade;

import com.artcode.designpatterns.structural.facade.restaurant.NormalRestaurant;
import com.artcode.designpatterns.structural.facade.restaurant.VeganRestaurant;
import com.artcode.designpatterns.structural.facade.restaurant.VegeRestaurant;

public class HotelKeeper {
    private VegeRestaurant vegeRestaurant;
    private VeganRestaurant veganRestaurant;
    private NormalRestaurant normalRestaurant;

    public HotelKeeper() {
        veganRestaurant = new VeganRestaurant();
        vegeRestaurant = new VegeRestaurant();
        normalRestaurant = new NormalRestaurant();
    }

    public String getVegeRestaurantMenu() {
        return vegeRestaurant.getMenu();
    }

    public String getVeganRestaurantMenu() {
        return veganRestaurant.getMenu();
    }

    public String getNormalRestaurantMenu() {
        return normalRestaurant.getMenu();
    }
}
