package com.artcode.designpatterns.structural.facade.restaurant;

public interface Restaurant {
    String getMenu();
}
