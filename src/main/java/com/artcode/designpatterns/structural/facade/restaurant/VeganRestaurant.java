package com.artcode.designpatterns.structural.facade.restaurant;

public class VeganRestaurant implements Restaurant {
    @Override
    public String getMenu() {
        return "Vegan restaurant menu";
    }
}
