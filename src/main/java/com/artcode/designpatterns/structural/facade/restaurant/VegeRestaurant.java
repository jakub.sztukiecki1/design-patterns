package com.artcode.designpatterns.structural.facade.restaurant;

public class VegeRestaurant implements Restaurant {
    @Override
    public String getMenu() {
        return "Vege restaurant menu";
    }
}
