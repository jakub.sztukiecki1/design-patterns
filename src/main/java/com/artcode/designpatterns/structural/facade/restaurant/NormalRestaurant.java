package com.artcode.designpatterns.structural.facade.restaurant;

public class NormalRestaurant implements Restaurant {
    @Override
    public String getMenu() {
        return "Normal restaurant menu";
    }
}
