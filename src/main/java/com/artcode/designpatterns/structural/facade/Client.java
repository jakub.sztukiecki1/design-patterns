package com.artcode.designpatterns.structural.facade;

public class Client {
    public static void main(String[] args) {
        HotelKeeper keeper = new HotelKeeper();
        System.out.println(keeper.getNormalRestaurantMenu());
        System.out.println(keeper.getVegeRestaurantMenu());
        System.out.println(keeper.getVeganRestaurantMenu());
    }
}
