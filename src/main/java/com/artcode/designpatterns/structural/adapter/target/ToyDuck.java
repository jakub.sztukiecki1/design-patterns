package com.artcode.designpatterns.structural.adapter.target;

public interface ToyDuck {
    void squeak();
}
