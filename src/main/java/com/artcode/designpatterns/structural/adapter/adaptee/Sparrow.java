package com.artcode.designpatterns.structural.adapter.adaptee;

public class Sparrow implements Bird {
    @Override
    public void makeSound() {
        System.out.println("Sparrow making a sound");
    }
}
