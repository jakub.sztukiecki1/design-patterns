package com.artcode.designpatterns.structural.adapter.target;

public class PlasticToyDuck implements ToyDuck {
    @Override
    public void squeak() {
        System.out.println("Squeaking");
    }
}
