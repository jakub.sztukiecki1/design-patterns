package com.artcode.designpatterns.structural.adapter;

import com.artcode.designpatterns.structural.adapter.adaptee.Bird;
import com.artcode.designpatterns.structural.adapter.target.ToyDuck;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class BirdAdapter implements ToyDuck {

    private Bird bird;

    @Override
    public void squeak() {
        bird.makeSound();
    }
}
