package com.artcode.designpatterns.structural.adapter.adaptee;

public interface Bird {
    void makeSound();
}
