package com.artcode.designpatterns.structural.adapter;

import com.artcode.designpatterns.structural.adapter.adaptee.Bird;
import com.artcode.designpatterns.structural.adapter.adaptee.Sparrow;

public class Client {
    public static void main(String[] args) {
        Bird bird = new Sparrow();
        BirdAdapter adapter = new BirdAdapter(bird);
        adapter.squeak();
    }
}
