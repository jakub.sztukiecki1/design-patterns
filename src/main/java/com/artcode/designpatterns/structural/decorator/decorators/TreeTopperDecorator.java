package com.artcode.designpatterns.structural.decorator.decorators;

import com.artcode.designpatterns.structural.decorator.ChristmasTree;

public class TreeTopperDecorator extends TreeDecorator {
    public TreeTopperDecorator(ChristmasTree tree) {
        super(tree);
    }

    @Override
    public String decorate() {
        return super.decorate() + decorateWithTopper();
    }

    private String decorateWithTopper() {
        return " with topper";
    }
}
