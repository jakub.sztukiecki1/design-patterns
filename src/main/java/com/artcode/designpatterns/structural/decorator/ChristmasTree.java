package com.artcode.designpatterns.structural.decorator;

public interface ChristmasTree {
    String decorate();
}
