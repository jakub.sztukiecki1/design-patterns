package com.artcode.designpatterns.structural.decorator.decorators;

import com.artcode.designpatterns.structural.decorator.ChristmasTree;

public class BubbleLightsDecorator extends TreeDecorator {
    public BubbleLightsDecorator(ChristmasTree tree) {
        super(tree);
    }

    @Override
    public String decorate() {
        return super.decorate() + decorateWithBubbleLights();
    }

    private String decorateWithBubbleLights() {
        return " with Bubble Lights";
    }
}
