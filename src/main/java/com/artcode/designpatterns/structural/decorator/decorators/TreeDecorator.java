package com.artcode.designpatterns.structural.decorator.decorators;

import com.artcode.designpatterns.structural.decorator.ChristmasTree;

public abstract class TreeDecorator implements ChristmasTree {

    private ChristmasTree tree;

    TreeDecorator(ChristmasTree tree) {
        this.tree = tree;
    }

    @Override
    public String decorate() {
        return tree.decorate();
    }
}
