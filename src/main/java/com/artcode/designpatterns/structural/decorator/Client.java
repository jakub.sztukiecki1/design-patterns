package com.artcode.designpatterns.structural.decorator;

import com.artcode.designpatterns.structural.decorator.decorators.BubbleLightsDecorator;
import com.artcode.designpatterns.structural.decorator.decorators.TreeTopperDecorator;

public class Client {

    public static void main(String[] args) {
        ChristmasTree tree = new BubbleLightsDecorator(
                new BubbleLightsDecorator(
                        new TreeTopperDecorator(
                                new ChristmasTreeImpl()
                        )
                )
        );
        System.out.println(tree.decorate());

        ChristmasTree tree2 = new BubbleLightsDecorator(
                new ChristmasTreeImpl()
        );
        System.out.println(tree2.decorate());
    }
}
