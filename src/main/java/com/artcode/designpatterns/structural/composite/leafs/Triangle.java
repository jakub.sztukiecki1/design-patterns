package com.artcode.designpatterns.structural.composite.leafs;

import com.artcode.designpatterns.structural.composite.Shape;

public class Triangle implements Shape {
    @Override
    public void draw(String fillColor) {
        System.out.println("Drawing triangle with color: " + fillColor);
    }
}
