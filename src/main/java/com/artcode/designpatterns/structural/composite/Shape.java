package com.artcode.designpatterns.structural.composite;

public interface Shape {
    void draw(String fillColor);
}
