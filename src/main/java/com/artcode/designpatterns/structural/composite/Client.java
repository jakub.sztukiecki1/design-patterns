package com.artcode.designpatterns.structural.composite;

import com.artcode.designpatterns.structural.composite.leafs.Circle;
import com.artcode.designpatterns.structural.composite.leafs.Triangle;

public class Client {

    public static void main(String[] args) {
        Shape triangle1 = new Triangle();
        Shape triangle2 = new Triangle();
        Shape circle1 = new Circle();

        Drawing drawing = new Drawing();
        drawing.add(triangle1);
        drawing.add(triangle2);
        drawing.add(circle1);

        drawing.draw("Red");

        drawing.clear();

        drawing.add(triangle1);
        drawing.add(circle1);
        drawing.draw("Green");
    }
}
