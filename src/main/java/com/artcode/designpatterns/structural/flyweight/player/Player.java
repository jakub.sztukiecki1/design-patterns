package com.artcode.designpatterns.structural.flyweight.player;

public interface Player {
    void assignWeapon(String weapon);
    void completeMission();
}
