package com.artcode.designpatterns.structural.flyweight;

import com.artcode.designpatterns.structural.flyweight.player.Player;

import java.util.ArrayList;
import java.util.List;

public class CounterStrike {
    public static void main(String[] args) {
        List<Player> counterTerrorist = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            counterTerrorist.add(PlayerFactory.getPlayer(PlayerFactory.PlayerType.COUNTER_TERRORIST));
            System.out.print(counterTerrorist.get(i) + " ");
            counterTerrorist.get(i).assignWeapon(" weapon " + i);
        }

        List<Player> terrorist = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            terrorist.add(PlayerFactory.getPlayer(PlayerFactory.PlayerType.TERRORIST));
            System.out.print(terrorist.get(i) + " ");
            terrorist.get(i).assignWeapon(" gets a weapon " + i);
        }

        for (int i = 0; i < 6; i++) {
            System.out.print(terrorist.get(i) + " ");
            terrorist.get(i).completeMission();
            System.out.print(counterTerrorist.get(i) + " ");
            counterTerrorist.get(i).completeMission();
        }
    }
}
