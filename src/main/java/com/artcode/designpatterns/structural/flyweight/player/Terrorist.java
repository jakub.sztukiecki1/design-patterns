package com.artcode.designpatterns.structural.flyweight.player;

public class Terrorist implements Player {

    private static final String MISSION = "PLANT A BOMB";

    @Override
    public void assignWeapon(String weapon) {
        System.out.println("Assigning a weapon: " + weapon);
    }

    @Override
    public void completeMission() {
        System.out.println("Completes mission: " + MISSION);
    }
}
