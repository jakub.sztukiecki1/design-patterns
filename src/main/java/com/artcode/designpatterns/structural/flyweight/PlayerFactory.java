package com.artcode.designpatterns.structural.flyweight;

import com.artcode.designpatterns.structural.flyweight.player.CounterTerrorist;
import com.artcode.designpatterns.structural.flyweight.player.Player;
import com.artcode.designpatterns.structural.flyweight.player.Terrorist;

import java.util.HashMap;

public class PlayerFactory {
    private static HashMap<PlayerType, Player> players = new HashMap<>();

    public static Player getPlayer(PlayerType playerType) {
        if (players.containsKey(playerType)) {
            return players.get(playerType);
        } else {
            switch (playerType) {
                case TERRORIST:
                    Terrorist terrorist = new Terrorist();
                    players.put(PlayerType.TERRORIST, terrorist);
                    return terrorist;
                case COUNTER_TERRORIST:
                    CounterTerrorist counterTerrorist = new CounterTerrorist();
                    players.put(PlayerType.COUNTER_TERRORIST, counterTerrorist);
                    return counterTerrorist;
                default:
                    throw new UnknownPlayerTypeException();
            }
        }
    }

    enum PlayerType {
        TERRORIST, COUNTER_TERRORIST
    }

    private static class UnknownPlayerTypeException extends RuntimeException {
    }
}
