# Design Patterns

### Creational design patterns
1) ##### Abstract factory
    Provides an interface for creating families of related or dependent objects without specifying their concrete classes.
    Main classes:
    - abstract factory - interface for creating abstract products (AbstractFactory),
    - concrete factory - implementation of creating concrete product (AnimalFactory, ColorFactory),
    - abstract product - interface for a product (Animal, Color),
    - concrete product - implementation fo a product (Dog, Duck, Red, Blue)
    - client - creates products using factories (Client).
2) ##### Builder
   Separates the construction of a complex object from its representation. Main classes:
    - builder - class responsible for object construction (UserBuilder),
    - product - represents a complex object under construction (User),
    - client - constructs a product using builder (Client).
3) ##### Factory Method
    Defines an interface for creating object, but let subclasses decide which class to instantiate. Main classes:
    - product - interface of object to be created (Shape),
    - concreteProduct - implementation of Product interface (Circle, Rectangle),
    - creator - declares the factory method which return an object of type Product (ShapeFactory).
    - concreteCreator - overrides the factory method to return an instance of a ConcreteProduct (Client).
4) ##### Prototype
    Creating new instances of class by copying the prototype. Main classes:
    - prototype - declares an interface for cloning itself (Cloneable),
    - concrete prototype - implements an operation for cloning itself (Employee),
    - client - creates a new object by asking a prototype to clone itself (Client).
5) ##### Singleton
    Ensures a class only has one instance and provides a global point of access to it. Main classes:
    - singleton - defines an instance that lets client access it.

### Structural design patterns
1) ##### Adapter
    Converts the interface of a class into another interface clients expect. Adapter lets classes work together that couln't otherwise because of incompatible interfaces. Main classes:
    - target - defines the domain-specific interface that client uses (ToyDuck),
    - client - collaborates with objects conforming to the target interface (Client),
    - adaptee - defines an existing interface that needs adapting (Bird),
    - adapter - adapts the interface of adaptee to the target interface (BirdAdapter).
2) ##### Bridge
    Decouples an abstraction from its implementation so that two can vary independently. Main classes:
    - abstraction - defines an abstraction's interface and maintains a reference to implementor (Shape),
    - abstraction implementation - implements the interface defined by abstraction and delegates logic to implementor (Triangle),
    - implementor - defines the interface for implementation classes (Color),
    - concrete implementor - implements the implementor interface and defines its concrete impelemtation (BlueColor).
3) ##### Composite
    Lets clients treat individual objects and compositions of objects uniformly. Clients can treat them in the same way. Main classes:
    - component - interface for objects in the composition, implements default behavior or the interface common to all classes (Shape),
    - leaf - represents leaf objects in the composition, defines behavior for primitives in composition - with no children (Circle, Triangle),
    - composite - defines behavior for components having children and stores children components (Drawing),
    - client - manipulates objects in the composition through the component interface (Client).
4) ##### Decorator
    Attaches additional responsibility to an object dynamically at runtime. Main classes:
    - component - defines the interface for objects that can have responsibilities added to them dynamically (ChristmasTree),
    - concrete component - defines an object to which additional responsibilities can be attached (ChristmasTreeImpl),
    - decorator - maintains a reference to a component object and and also implements component's interface (TreeDecorator),
    - concrete decorator - adds responsibilities to the component (BubbleLightsDecorator, TreeTopperDecorator).
5) ##### Facade
    Provides a unified interface to a set of interfaces in a subsystem. Defines a higher-level interface that makes the subsystem easier to use. Main classes:
    - facade - delegates client requests to appropriate subsystem objects,
    - subsystem classes - implement subsystem functionality and handle work assigned by the facade.
    - client - uses subsystem functionalities indirectly by using facade.
6) ##### Flyweight
    Uses sharing to support large numbers of small objects efficiently. The internal state of shared objects is the same, but the external state may vary (it's passed via parameters to methods). Main classes:
    - flyweight - declares an interface through which flyweights can receive and act on external state (Player),
    - concrete flyweight - implements the flyweight interface and adds storage for internal state. Any state it stores is internal (Terrorist, CounterTerrorist),
    - flyweight factory - creates and manages flyweights objects. When a client requests a flyweight, the factory object supplies an existing instance or creates one, if none exists (PlayerFactory),
    - client - maintains a reference to flyweights. Computes or stores the external state of flyweights (CounterStrike).
7) ##### Proxy
    Provides a surrogate or a placeholder for another object to control access to it. Main classes:
    - proxy - maintains a reference to the real subject. Provides an interface identical as subject so that a proxy can be substituted for the real subject. Proxy controls access to the real subject (ResourceProxy).
    - subject - defines the interface for real subject and proxy so that proxy can be used anywhere a real subject is expected (Resource),
    - real subject - defines the real object that the proxy represents (FileResource).
### Behavioral design patterns
1) ##### Chain of responsibility
    Avoids coupling the sender of a request to its receiver by giving more than one object a chain to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it. Main classes:
    - handler - defines an interface for handling requests. Optionally implements the successor link (DispenseChain),
    - concrete handler - handles requests it is responsible for, forwards the request to its successor when it wants to (Dollar10Dispenser, Dollar20Dispenser, Dollar50Dispenser),
    - client - initiates the request to a concrete handler object (ATM).
2) ##### Command
    Encapsulates a request as an object letting you parametrize clients with different request and support undoable operations. Main classes:
    - command - declares an interface for executing an operation (Command),
    - concrete command - defines a binding between a receiver object and an action. Implements execute by invoking corresponding operation on receiver (LightsOnCommand, LightsOfCommand),
    - invoker - asks the command to carry out the request (RemoteControl),
    - receiver - knows how to perform the operations associated with the request (Light),
    - client - creates a concrete command object and sets its receiver (Client).
3) ##### Interpreter
    Defines a representation for some language grammar and interpreter that uses the representation to interpret sentences in the language. Main classes:
    - AbstractExpression - declares an abstract interpret operation that is common to all nodes in the abstract syntax tree (Expression),
    - TerminalExpression - implements an interpret operation associated with terminal symbols in the grammar. An instance is required for every terminal symbol in a sentence (NumberExpression),
    - NonTerminalExpression - one such class is required for every rule R = R1 * R2 * R3 * ... * Rn in the grammar. Maintains instance variables of type AbstractExpression for each symbols R1 through Rn. Implements an interpret operation for non terminal symbols in the grammar. IInterpret typically calls itself recursively on the variables representing R1 through Rn (AdditionExpression, SubtractionExpression),
    - Context - contains information that's global to the interpreter (String),
    - Client - builds an abstract syntax tree representing a particular sentence in the language that the grammar defines. The abstract syntax tree is assembled from instances of NonTerminalExpression and TerminalExpression. Invokes the Interpret operation (Client).
4) ##### Iterator
    Provides a way to access the elements of an aggregate object sequentially without exposing its representation. Main classes:
    - Iterator - defines an interface for accessing and traversing elements (ChannelIterator),
    - ConcreteIterator - implements the iterator interface. Keeps track of the current position in the traversal of the aggregate (ChannelIteratorImpl),
    - Aggregate - defines an interface for creating iterator object (ChannelCollection),
    - ConcreteAggregate - implements the iterator creation interface to return an instance of the proper concrete iterator (ChannelCollectionImpl).
5) ##### Mediator
    Defines an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects from referring to each other explicitly. Main classes:
    - Mediator - defines an interface for communicating with Colleague objects,
    - ConcreteMediator - implements cooperative behavior by coordinating Colleague objects, knows and maintains its colleagues (DialogMediator),
    - Colleague classes - each Colleague class knows its Mediator object and communicates with other colleagues through Mediator object (Background, Button, Text).
6) ##### Memento
    Without violating encapsulation, capture and externalize an object's internal state so that the object can be restored to this state later. Main classes:
    - Memento - stores internal state of the Originator object. It stores as much internal state as necessary (TextFieldMemento),
    - Originator - creates a memento containing a snapshot of its current internal state. Uses the memento to restore its internal state (TextField),
    - Caretaker - responsible for the memento's safekeeping. Never operates on or examines the contents of a memento (TextFieldMementoCaretaker).
7) ##### Observer
    Defines a one to many dependency between objects so that when one object changes state, all its subscribers are notified. Main classes:
    - Subject - knows its observers and provides an interface for attaching and detaching Observer objects (Observable).
    - Observer - defines an interface for objects that should be notified of changes in a subject (Observer).
    - ConcreteSubject - sends a notification to its observers when its state changes (NewsAgency).
    - ConcreteObserver - maintains a reference to a ConcreteSubject and listens to changes on the Subject (NewsChannel).
8) ##### State
    Allows an object to alter its behavior when its internal state changes. The object will appear to change its class. Main classes:
    - Context - defines the interface tof interest to clients. Maintains an instance of a CocreteState subclass that defines the current state (MP3Player),
    - State - defines an interface for encapsulating the behavior associated with a particular state of the Context (State),
    - ConcreteState - each subclass implements a behavior associated with a state of the Context (StandbyState, PlayingState).
9) ##### Strategy
    Defines a family of algorithms, encapsulates each one, and makes them interchangeable. Strategy lets the algorithm vary indepedently from clients that use it. Main classes:
    - Strategy - declares an interface common to all supported algorithms. Context uses this interface to call the algorithm defined by a ConcreteStrategy (BonusGivingStrategy),
    - ConcreteStrategy - implements the algorithm using the Strategy interface (ChristmasBonusGivingStrategy, GenerousBonusGivingStrategy, StingyBonusGivingStrategy),
    - Context - is configured with a ConcreteStrategy object. Maintains a reference to a Strategy object (Employee).
10) ##### Template method
    Defines the skeleton of an algorithm and lets subclasses redefine certain steps of it without changing the algorithm's structure. Main classes:
    - AbstractClass - defines abstract primitve operations that concrete subclasses define to implement steps of algorithm. Implements a template method defining the skeleton of an algorithm (HouseTemplate),
    - ConcreteClass - implements the primitive operations to carry out subclass-specific steps of the algorithm (WoodenHouse, GlassHouse).
11) ##### Visitor
    Represents an operation to be performed on the elements of an object structure. Visitor lets you define a new operation without changing the classes tf the elements on which it operates. Main classes:
    - Visitor - declares a visit operation for each class of ConcreteElement in the object structure. The operation's name and signature identifiers the class that sends the Visit request to the visitor (Visitor),
    - Concrete Visitor - implements each operation declared by Visitor. Each operation implements a fragment of the algorithm defined for the corresponding class of object in the structure (XMLExportVisitor),
    - Element - defines an accept operation that takes a visitor as an argument (Shape),
    - Concrete Element - implements an accept operation that takes a visitor as an argument (Circle, Rectangle),
    - Object Structure - can enumerate its elements. May provide a high-level interface to allow the visitor to visit its elements. May be either a composite or a collection (List).
